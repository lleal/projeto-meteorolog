# Projeto de desenvolvimento do shield das estações meteorológicas Meteorolog #

Esquemáticos e bibliotecas publicados sob licença  
[CERN-OHL](http://www.ohwr.org/projects/cernohl/wiki).

Salvo a biblioteca 'Arduino Shield Modules for KiCAD V3',
publicada por [nicholasclewis](http://www.thingiverse.com/thing:9630) sob licença CC-BY

Desenvolvido por Lucas Leal em contribuição ao projeto do  
[Centro de Tecnologia Acadêmica da UFRGS](http://cta.if.ufrgs.br)
